from .subprocess_compat import TimeoutExpired, Popen, PIPE
import re
import logging
from . import mails
from collections import Iterable
from datetime import datetime


class Host(object):
    def __init__(self, ipv4='undefined', ipv6='undefined', name=None):
        self.ipv4 = ipv4
        self.ipv6 = ipv6
        self.name = name if name is not None else "%s/%s" % (ipv4, ipv6)


class Checks(list):
    def add(self, checks, dests, **options):
        if not isinstance(checks, Iterable):
            checks = [checks]
        if not isinstance(dests, Iterable):
            dests = [dests]
        for check in checks:
            self += [check(d, **options) for d in dests]

    # this should (assuming "other" is the "older" list):
    # - pickup checks defined in both list (keep old one with its variables)
    # - email for checks in the other list but not in us (it was removed)
    def merge(self, other):
        for oldcheck in other:
            found = False
            for idx, newcheck in enumerate(self):
                if oldcheck == newcheck:
                    self[idx] = oldcheck
                    found = True
            if not found and not oldcheck.ok:
                mails.send_email_for_check(oldcheck, True)


class Check(object):
    def __init__(self, dest, **options):
        from . import config
        self.dest        = dest
        self._options    = options
        self.retry       = options.get('retry', 1)
        self.retry_count = 0
        self.every       = options.get('every', config.default_every)
        self.error_every = options.get('error_every', config.default_error_every)
        if self.error_every < 0:
            self.error_every = self.every
        self.run_count   = 0
        self.errmsg      = ''
        self.last_exec   = ''
        self.ok          = True
        self.target_name = options.pop('target_name', dest)
        self.timeout     = options.get('timeout', 2)

    def __repr__(self):
        return '<{}, {}({}) with {} (N={}/{}, R={}/{})>'.format(self.target_name,
                                                     self.__class__.__name__,
                                                     self.dest,
                                                     self._options,
                                                     self.run_count,
                                                     self.every,
                                                     self.retry_count,
                                                     self.retry)

    def __eq__(self, other):
        return (self.__class__.__name__ == other.__class__.__name__ and
                self.dest               == other.dest               and
                self._options           == other._options)

    def setup(self):
        pass

    def teardown(self):
        pass

    def check(self, host, addr):
        pass

    def run(self, immediate=False):
        self.run_count = (self.run_count + 1) % (
                          self.every if self.ok else self.error_every)
        if self.run_count == 0 or immediate:
            logging.debug('Running ' + str(self))
            self.setup()
            if not self.check():
                logging.debug('Fail: ' + str(self))
                self.retry_count += 1
                if self.retry_count >= self.retry or immediate:
                    if self.ok:
                        logging.debug('Switched to failure: ' + str(self))
                        self.failure_date = datetime.now()
                        self.ok = False
                        mails.send_email_for_check(self)
            else:
                logging.debug('OK: ' + str(self))
                if not self.ok:
                    logging.debug('Switched to ok: ' + str(self))
                    self.ok = True
                    mails.send_email_for_check(self)
                self.retry_count = 0
            self.teardown()
        return self.ok

    def exec_with_timeout(self, command, timeout=None, pattern=''):
        timeout = self.timeout if timeout is None else timeout
        self.errmsg    = ''
        self.last_exec = ' '.join(map(
                            lambda s: "'" + s.replace("'", "'\"'\"'") + "'",
                            command))
        try:
            p = Popen(map(str, command), stdout=PIPE, stderr=PIPE)
        except OSError as e:
            self.errmsg = 'Check not available: ' + e.strerror
            return False
        try:
            out, err = p.communicate(timeout=timeout)
        except TimeoutExpired:
            p.kill()
            out, err = p.communicate()
            self.errmsg += "Operation timed out\n"
            return False
        if p.returncode != 0:
            if len(out) > 0:
                self.errmsg += "stdout:\n" + \
                               out.decode(errors='replace') + '\n'
            if len(err) > 0:
                self.errmsg += "stderr:\n" + \
                               err.decode(errors='replace') + '\n'
        if re.search(pattern, str(out), flags=re.M) is None:
            self.errmsg += ("Pattern '%s' not found in reply.\nstdout: %s"
                            % (pattern, out.decode(errors='replace')))
            return False
        return p.returncode == 0


class Check4(Check):
    def __init__(self, host, **options):
        options.setdefault('target_name', host.name)
        super().__init__(host.ipv4, **options)


class Check6(Check):
    def __init__(self, host, **options):
        options.setdefault('target_name', host.name)
        super().__init__(host.ipv6, **options)


class CheckPing4(Check4):
    def check(self):
        command = ['/bin/ping', '-c', '1', '-W', str(self.timeout), self.dest]
        return self.exec_with_timeout(command, timeout=self.timeout + 1)


class CheckPing6(Check6):
    def check(self):
        command = ['/bin/ping6', '-c', '1', '-W', str(self.timeout), self.dest]
        return self.exec_with_timeout(command, timeout=self.timeout + 1)


class CheckDNSZone(Check):
    def __init__(self, zone, **options):
        options.setdefault('target_name', 'zone ' + zone)
        super().__init__(zone, **options)

    def check(self):
        command = ['check_dns_soa', '-H', self.dest]
        if self._options.get('ip_version', 0) in [4, 6]:
            command.append('-' + str(self._options['ip_version']))
        return self.exec_with_timeout(command)


class CheckDNSRec(Check):
    def check(self):
        command = ['dig', 'www.google.com', '@' + self.dest]
        return self.exec_with_timeout(command, pattern='status: NOERROR')


class CheckDNSRec4(CheckDNSRec, Check4):
    pass


class CheckDNSRec6(CheckDNSRec, Check6):
    pass


class CheckDNSAut(Check):
    def check(self, host, addr):
        self.errmsg = "Unimplemented"
        return False


class CheckHTTP(Check):
    def build_command(self):
        command = ['/usr/lib/nagios/plugins/check_http',
                   '-I', self.dest, '-t', str(self.timeout)]
        if 'status' in self._options:
            command += ['-e', str(self._options['status'])]
        if 'vhost' in self._options:
            command += ['-H', str(self._options['vhost'])]
        if 'string' in self._options:
            command += ['-s', str(self._options['string'])]
        if 'auth' in self._options:
            command += ['-a', str(self._options['auth'])]
        if 'url' in self._options:
            command += ['-u', str(self._options['url'])]
        return command

    def check(self):
        command = self.build_command()
        return self.exec_with_timeout(command, timeout=self.timeout + 1)


class CheckHTTPS(CheckHTTP):
    def check(self):
        command = self.build_command() + ['--ssl', '--sni']
        return self.exec_with_timeout(command, timeout=self.timeout + 1)


class CheckHTTP4(CheckHTTP, Check4):
    pass


class CheckHTTP6(CheckHTTP, Check6):
    pass


class CheckHTTPS4(CheckHTTPS, Check4):
    pass


class CheckHTTPS6(CheckHTTPS, Check6):
    pass


class CheckSMTP(Check):
    def build_command(self):
        command = ['/usr/lib/nagios/plugins/check_smtp',
                   '-H', self.dest,
                   '-f', self._options.get('from_addr',
                                           'picomon@localhost.local'),
                   '-t', str(self.timeout)]
        if 'command' in self._options:
            command += ['-C', str(self._options['command'])]
        if 'response' in self._options:
            command += ['-R', str(self._options['response'])]
        return command

    def check(self):
        command = self.build_command()
        return self.exec_with_timeout(command, timeout=self.timeout + 1)


class CheckSMTP4(CheckSMTP, Check4):
    pass


class CheckSMTP6(CheckSMTP, Check6):
    pass


class CheckOpenVPN(Check):
    def build_command(self):
        command = ['/usr/lib/nagios/plugins/check_openvpn',
                   '-p', "443",
                   '--timeout', str(self.timeout),
                   str(self.dest)]
        return command

    def check(self):
        command = self.build_command()
        return self.exec_with_timeout(command, timeout=self.timeout + 1)


class CheckOpenVPN4(CheckOpenVPN, Check4):
    pass


class CheckOpenVPN6(CheckOpenVPN, Check6):
    pass


class CheckOpenVPNTCP(CheckOpenVPN):
    def check(self):
        command = self.build_command()
        command.insert(1, '-t')
        return self.exec_with_timeout(command, timeout=self.timeout + 1)


class CheckOpenVPNTCP4(CheckOpenVPNTCP, Check4):
    pass


class CheckOpenVPNTCP6(CheckOpenVPNTCP, Check6):
    pass


class CheckJabber(Check):
    def check(self):
        command = ['/usr/lib/nagios/plugins/check_jabber',
                   '-H', self.dest,
                   '-t', str(self.timeout)]
        return self.exec_with_timeout(command, timeout=self.timeout + 1)


class CheckJabber4(CheckJabber, Check4):
    pass


class CheckJabber6(CheckJabber, Check6):
    pass


class CheckTLSCert(Check):
    def build_command(self):
        command = ['/usr/lib/nagios/plugins/check_http',
                   '--ssl', '--sni',
                   '-C', str(self._options.get('warn', 7)),
                   '-t', str(self.timeout)]
        if 'port' in self._options:
            command += ['-p', str(self._options['port'])]
        if 'vhost' in self._options:
            command += ['-H', str(self._options['vhost'])]
        command += [self.dest]
        return command

    def check(self):
        command = self.build_command()
        return self.exec_with_timeout(command, timeout=self.timeout + 1)


class CheckTLSCert4(CheckTLSCert, Check4):
    pass


class CheckTLSCert6(CheckTLSCert, Check6):
    pass


class CheckSSH(Check):
    def check(self):
        command = ['/usr/lib/nagios/plugins/check_ssh',
                   '-H', self.dest,
                   '-t', str(self.timeout)]
        if 'port' in self._options:
            command += ['-p', str(self._options['port'])]
        return self.exec_with_timeout(command, timeout=self.timeout + 1)


class CheckSSH4(CheckSSH, Check4):
    pass


class CheckSSH6(CheckSSH, Check6):
    pass
