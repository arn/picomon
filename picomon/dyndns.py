import socket
from .checks import Host


class DynDNS(object):
    """
    mimicks a str() but resolves a host to an IP

    >>> x=DynDNS('localhost')
    >>> print(x)
    127.0.0.1
    >>> print(x.replace('.', ' '))
    127 0 0 1
    >>> x == 'localhost'
    True
    >>> x == '127.0.0.1'
    True
    """

    def __init__(self, host, family=0):
        self._host = host
        self._family = family

    def __str__(self):
        """ pick up the first matching address """
        return socket.getaddrinfo(self._host, None, self._family)[0][4][0]

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        """ consider equality if host or string representation match """
        if isinstance(other, type(self)) and self._host == other._host:
            return True

        other_str = str(other)
        return self._host == other_str or str(self) == other_str

    def __getattr__(self, attr):
        return getattr(str(self), attr)


class DynDNS4(DynDNS):
    def __init__(self, host):
        DynDNS.__init__(self, host, socket.AF_INET)


class DynDNS6(DynDNS):
    def __init__(self, host):
        DynDNS.__init__(self, host, socket.AF_INET6)


class DynHost(Host):
    def __init__(self, dns, ipv4=None, ipv6=None, name=None):
        Host.__init__(self, ipv4=ipv4 or DynDNS4(dns),
                      ipv6=ipv6 or DynDNS6(dns), name=name or dns)


class DynHost4(Host):
    def __init__(self, dns, name=None):
        Host.__init__(self, ipv4=DynDNS4(dns), name=name or dns)


class DynHost6(Host):
    def __init__(self, dns):
        Host.__init__(self, ipv6=DynDNS6(dns), name=name or dns)
